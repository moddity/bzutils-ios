//
//  UIImage+BZImage.h
//  Versus
//
//  Created by Jaume Cornadó on 19/12/13.
//  Copyright (c) 2013 mOddity. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (BZImage)

- (UIImage *)scaleImageToSize:(CGSize)size;
- (UIImage *)scaleImageProportionallyToSize:(CGSize)newSize;

@end

//
//  SQLiteManager.h
//  
//
//  Created by Oriol Vilaró on 19/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"

#define SQLITE_FILE @"solmusica.db"

@interface SQLiteManager : NSObject


+(void)createEditableCopyOfDatabaseIfNeeded;

+(NSArray*)executeSentences:(NSArray *)sentences 
              withWildcards:(NSArray *)wildcards
           sentenceIsSelect:(BOOL )isSelect 
               numberColums:(NSInteger)columns;

+(NSArray*)getSections;

+(NSArray*)getVideosOfSection:(NSString*)category
                    withLimit:(NSInteger)limit;

+(NSArray*)getThumbAndRateForVideo:(NSString*)videoId;

+(NSArray*)searchVideoWithString:(NSString*)searchString;

@end

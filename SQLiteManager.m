//
//  SQLiteManager.m
//  
//
//  Created by Oriol Vilaró on 19/09/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "SQLiteManager.h"

@implementation SQLiteManager

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

+(void)createEditableCopyOfDatabaseIfNeeded {
    
	BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:SQLITE_FILE];
    success = [fileManager fileExistsAtPath:writableDBPath];
	
	// Si ya existe el archivo, no lo crea -_-
    if (success) return;
    
	// Crea el archivo en el dispositivo
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:SQLITE_FILE];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}

+(NSArray*)executeSentences:(NSArray *)sentences
              withWildcards:(NSArray *)wildcards
           sentenceIsSelect:(BOOL )isSelect 
               numberColums:(NSInteger)columns{
	
	// Variables para realizar la consulta
	static sqlite3 *db;     
	sqlite3_stmt *resultado; 
	const char* siguiente; 
    
    NSMutableArray *queryArray = [[NSMutableArray alloc] init];
    
	// Buscar el archivo de base de datos
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:SQLITE_FILE];
    
	// Abre el archivo de base de datos
	if (sqlite3_open([path UTF8String], &db) == SQLITE_OK) {		
		
		if (isSelect){	
			
            NSString *sentence = [sentences objectAtIndex:0];
            
			// Ejecuta la consulta
			if ( sqlite3_prepare(db,[sentence UTF8String],[sentence length],&resultado,&siguiente) == SQLITE_OK ){
                
                if (wildcards!=nil) {
                    
                    for (int i=0; i<[wildcards count] ; i++ ) {
                        NSString *wildcard = [wildcards objectAtIndex:i];
                        sqlite3_bind_text(resultado, (i+1), [wildcard UTF8String], -1, SQLITE_STATIC);
                    }
                }    
                
				// Recorre el resultado
				while (sqlite3_step(resultado)==SQLITE_ROW){	
                    
                    NSMutableArray *rowArray=[[NSMutableArray alloc] initWithCapacity:columns];
                    
                    for (int i=0; i<columns; i++) {
                        
                        [rowArray addObject:[NSString stringWithUTF8String: (char *)sqlite3_column_text(resultado, i)]];
                    }
                    
                    [queryArray addObject:rowArray];
                 
                    
				}
			}else{
                
                NSLog(@"sqlite3_prepare ko");
            }
		}
		else {
            
            sqlite3_exec(db, "BEGIN", 0, 0, 0);
            
            NSUInteger total_sentences = [sentences count];
            NSUInteger ok_sentences = 0;
            
            for (NSString *sentence in sentences) {
                
//                NSLog(@"%@",sentence);
                
                // Ejecuta la consulta
                if ( sqlite3_prepare_v2(db,[sentence UTF8String],-1,&resultado,&siguiente) == SQLITE_OK ){
                    sqlite3_step(resultado);				
                    sqlite3_finalize(resultado);
                    
                    ok_sentences+=1;
                    
                }else{
                    
                    NSLog(@"sqlite3_prepare_v2 ko: %@",sentence);
                }
            }
            
            sqlite3_exec(db, "COMMIT", 0, 0, 0);
            
            NSLog(@"total sentences: %d",total_sentences);
            NSLog(@"ok sentences...: %d",ok_sentences);
            
		}
	}else{
        
        NSLog(@"sqlite3_open ko");
    }
	// Cierra el archivo de base de datos
	sqlite3_close(db);
    
    NSArray *returnArray=[NSArray arrayWithArray:queryArray];
    

    	
    return returnArray;
}


@end

//
//  BZFormValidator.h
//  Galf
//
//  Created by Jaume Cornadó on 15/07/13.
//  Copyright (c) 2013 Bazinga Systems. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    kRequiredValidator
} kValidationType;

@interface BZFormValidator : NSObject


+(void) validateFields: (NSArray*) fields withValidation: (NSArray*) validationRules;

@end

//
//  NonSelectableUITextView.h
//  Pronovias
//
//  Created by Jaume Cornadó on 04/11/13.
//
//

#import <UIKit/UIKit.h>

@interface NonSelectableUITextView : UITextView

@end

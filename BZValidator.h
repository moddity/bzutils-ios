//
//  BZValidator.h
//  
//
//  Created by Oriol Vilaró on 05/09/11.
//  Copyright (c) 2011 Bazinga Systems S.L.. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define CHARACTERS_NUMBERS [CHARACTERS stringByAppendingString:@"1234567890"]

@interface BZValidator : NSObject

//isNum
+(BOOL) isNum:(NSString*)string;

//min lenght...
+(BOOL) checkLenght:(NSString*)string
          minLenght:(NSInteger)minLenght 
          maxLenght:(NSInteger)maxLenght;

//isEmailValid...
//i al mail que tngui un @
+(BOOL) isEmailValid:(NSString*)string;

//aqui nomes validem k siguin 5 numreos
//i que estigui per sota d 53000
+(BOOL) isPostalCodeValid:(NSString*)string;

//Als camps de text es mira que no es fiquin caràcters estranys com ^!<>?=+@{}_$%
+(BOOL) isTextValid:(NSString*)string;

+(BOOL) isPhoneNumberValid:(NSString*)string;
//Al número de telèfon prestashop es traga números i els caràcters +. ()#-, i amb teclat numèric molt millor, s ha d mirar la longitud de 9 i si voleu que comenci per 6 o 9

//comprova si la lletra del dni/nie introduida es correcta.
+(BOOL) isDocumentoIdentidadValido:(NSString*) string
                             isNIE:(BOOL) isNie;

//El NSError detalla l'error, esta fet a través de l'article de la wikipedia
+(BOOL) isCIFValid:(NSString *) cif error:(NSError**)error;

// no funciona per tots els casos
//+(BOOL) isCIFValid:(NSString*) cif;

+(NSString*) checkDocumentLetter:(NSString*) numbers;

+(BOOL) areAllCharactersOfThisString:(NSString*)string
                    inThisCollection:(NSString*)characters;

@end

//
//  BZValidator.m
//  
//
//  Created by Oriol Vilaró on 05/09/11.
//  Copyright (c) 2011 Bazinga Systems S.L.. All rights reserved.
//

#import "BZValidator.h"

@implementation BZValidator

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

+(BOOL) isNum:(NSString*)string{
    
    NSScanner *sc = [NSScanner scannerWithString: string];
    if ( [sc scanFloat:NULL] )
    {
        return [sc isAtEnd];
    }
    return NO;
}

+(BOOL) checkLenght:(NSString *)string minLenght:(NSInteger)minLenght maxLenght:(NSInteger)maxLenght{  
    
    if ([string length]>=minLenght && [string length]<=maxLenght) {
        return YES;
    }
    
    return NO;
}    

+(BOOL) isEmailValid:(NSString*)string{
    
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:string];
}

+(BOOL) isPostalCodeValid:(NSString*)string{
    
    if ([self isNum:string] && [self checkLenght:string minLenght:5 maxLenght:5]) {
        
        if ([string intValue]<=53000) {
            return YES;
        }
    }
    
    return NO;
}

+(BOOL) isTextValid:(NSString*)string{
    
    NSCharacterSet *charSet=[NSCharacterSet characterSetWithCharactersInString:@"^!<>?=+@{}_$%"];
    
    NSRange range=[string rangeOfCharacterFromSet:charSet];
    
    if (range.location == NSNotFound){
        return YES;
    }
    
    return NO;
}

+(BOOL) isPhoneNumberValid:(NSString*)string{
    
    if ([self isNum:string] && [self checkLenght:string minLenght:9 maxLenght:9]) {
        
        if ([[string substringToIndex:1] isEqualToString:@"6"] || [[string substringToIndex:1] isEqualToString:@"7"] || [[string substringToIndex:1] isEqualToString:@"9"]) {
            
            return YES;
        }
    }
   
    return NO;
}



+(BOOL) isDocumentoIdentidadValido:(NSString*) string  isNIE:(BOOL) isNie{
    
    if ([string length] == 9) {
        
        NSString* numerosStr;
        NSString* lastLetter = [string substringFromIndex:[string length]-1];
        
        if (isNie) {
            NSString* firstLetter = [string substringToIndex:1];
            NSCharacterSet *charSet=[NSCharacterSet characterSetWithCharactersInString:@"XYZ"];
            
            NSRange range=[firstLetter rangeOfCharacterFromSet:charSet];
            
            if (range.location != NSNotFound){
                 numerosStr = [string substringWithRange:NSMakeRange(1, 7)];
                if ([firstLetter isEqualToString:@"X" ]) {
                    numerosStr = [@"0" stringByAppendingString:numerosStr];
                } else  if ([firstLetter isEqualToString:@"Y" ]) {
                    numerosStr = [@"1" stringByAppendingString:numerosStr];
                } else  if ([firstLetter isEqualToString:@"Z" ]) {
                    numerosStr = [@"2" stringByAppendingString:numerosStr];
                }
                if ([self isNum:numerosStr]) {
                    if ([lastLetter isEqualToString:[self checkDocumentLetter:numerosStr]]) {
                        return YES;
                    }
                }
            }
        } else {
            numerosStr = [string substringToIndex:[string length]-1];
            if ([self isNum:numerosStr]) {
                if ([lastLetter isEqualToString:[self checkDocumentLetter:numerosStr]]) {
                    return YES;
                }
                
            }

        }
        
       
    }
    return NO;
}


+(BOOL) isCIFValid:(NSString *) cif error:(NSError**)error {
    
    // http://es.wikipedia.org/wiki/Código_de_identificación_fiscal
    
    BOOL cifCorrecto = true;
    NSString * errorMsg;
    
    if ([cif length]==9) {
        //1º  tipo 1 caracter
        NSString *tipoCIF = [cif substringWithRange:NSMakeRange(0, 1)];
        
        //posibles caracteres
        NSCharacterSet *cset = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHJKLMNPQRSVWabcdefghjklmnpqrsvw"];
        NSRange range = [tipoCIF rangeOfCharacterFromSet:cset];
        if (range.location == NSNotFound) {
            //El caracter no es vàlido
            cifCorrecto = false;
            errorMsg = @"El 1º Caracter no és válido";
        } else { // 1º Parte OK
            //2º la provincia 2 digitos
            NSString *provinciaCIF = [cif substringWithRange:NSMakeRange(1, 2)];
            
            //Provincia del CIF facilitado
            NSString * provincia = @"";
            
            if ([provinciaCIF isEqualToString:@"01"]) {
                provincia = @"Álava";
            } else if ([provinciaCIF isEqualToString:@"02"]) {
                provincia = @"Albacete";
            } else if ([provinciaCIF isEqualToString:@"03"] || [provinciaCIF isEqualToString:@"53"] || [provinciaCIF isEqualToString:@"54"]) {
                provincia = @"Alicante";
            } else if ([provinciaCIF isEqualToString:@"04"]) {
                provincia = @"Almería";
            } else if ([provinciaCIF isEqualToString:@"05"]) {
                provincia = @"Ávila";
            } else if ([provinciaCIF isEqualToString:@"06"]) {
                provincia = @"Badajoz";
            } else if ([provinciaCIF isEqualToString:@"07"] || [provinciaCIF isEqualToString:@"57"]) {
                provincia = @"Islas Baleares";
            } else if ([provinciaCIF isEqualToString:@"08"] || [provinciaCIF isEqualToString:@"58"] || [provinciaCIF isEqualToString:@"59"] || [provinciaCIF isEqualToString:@"60"] || [provinciaCIF isEqualToString:@"61"] || [provinciaCIF isEqualToString:@"62"] || [provinciaCIF isEqualToString:@"63"] || [provinciaCIF isEqualToString:@"64"] || [provinciaCIF isEqualToString:@"65"] || [provinciaCIF isEqualToString:@"66"] || [provinciaCIF isEqualToString:@"68"]) {
                provincia = @"Barcelona";
            } else if ([provinciaCIF isEqualToString:@"09"]) {
                provincia = @"Burgos";
            } else if ([provinciaCIF isEqualToString:@"10"]) {
                provincia = @"Cáceres";
            } else if ([provinciaCIF isEqualToString:@"11"] || [provinciaCIF isEqualToString:@"72"]) {
                provincia = @"Cádiz";
            } else if ([provinciaCIF isEqualToString:@"12"]) {
                provincia = @"Castellón";
            } else if ([provinciaCIF isEqualToString:@"13"]) {
                provincia = @"Ciudad Real";
            } else if ([provinciaCIF isEqualToString:@"14"] || [provinciaCIF isEqualToString:@"56"]) {
                provincia = @"Córdoba";
            } else if ([provinciaCIF isEqualToString:@"15"] || [provinciaCIF isEqualToString:@"70"]) {
                provincia = @"La Coruña";
            } else if ([provinciaCIF isEqualToString:@"16"]) {
                provincia = @"Cuenca";
            } else if ([provinciaCIF isEqualToString:@"17"] || [provinciaCIF isEqualToString:@"55"]) {
                provincia = @"Girona";
            } else if ([provinciaCIF isEqualToString:@"18"] || [provinciaCIF isEqualToString:@"19"]) {
                provincia = @"Granada";
            } else if ([provinciaCIF isEqualToString:@"19"]) {
                provincia = @"Guadalajara";
            } else if ([provinciaCIF isEqualToString:@"20"]) {
                provincia = @"Guipúzcuoa";
            } else if ([provinciaCIF isEqualToString:@"21"]) {
                provincia = @"Huelva";
            } else if ([provinciaCIF isEqualToString:@"22"]) {
                provincia = @"Huesca";
            } else if ([provinciaCIF isEqualToString:@"23"]) {
                provincia = @"Jaén";
            } else if ([provinciaCIF isEqualToString:@"24"]) {
                provincia = @"León";
            } else if ([provinciaCIF isEqualToString:@"25"]) {
                provincia = @"Lleida";
            } else if ([provinciaCIF isEqualToString:@"26"]) {
                provincia = @"La Rioja";
            } else if ([provinciaCIF isEqualToString:@"27"]) {
                provincia = @"Lugo";
            } else if ([provinciaCIF isEqualToString:@"28"] || [provinciaCIF isEqualToString:@"78"] || [provinciaCIF isEqualToString:@"79"] || [provinciaCIF isEqualToString:@"80"] || [provinciaCIF isEqualToString:@"81"] || [provinciaCIF isEqualToString:@"82"] || [provinciaCIF isEqualToString:@"83"] || [provinciaCIF isEqualToString:@"84"] || [provinciaCIF isEqualToString:@"85"] || [provinciaCIF isEqualToString:@"86"]) {
                provincia = @"Madrid";
            } else if ([provinciaCIF isEqualToString:@"29"] || [provinciaCIF isEqualToString:@"92"] || [provinciaCIF isEqualToString:@"93"]) {
                provincia = @"Málaga";
            } else if ([provinciaCIF isEqualToString:@"30"] || [provinciaCIF isEqualToString:@"73"]) {
                provincia = @"Murcia";
            } else if ([provinciaCIF isEqualToString:@"31"] || [provinciaCIF isEqualToString:@"71"]) {
                provincia = @"Navarra";
            } else if ([provinciaCIF isEqualToString:@"32"]) {
                provincia = @"Orense";
            } else if ([provinciaCIF isEqualToString:@"33"] || [provinciaCIF isEqualToString:@"74"]) {
                provincia = @"Asturias";
            } else if ([provinciaCIF isEqualToString:@"34"]) {
                provincia = @"Palencia";
            } else if ([provinciaCIF isEqualToString:@"35"] || [provinciaCIF isEqualToString:@"76"]) {
                provincia = @"Las Palmas";
            } else if ([provinciaCIF isEqualToString:@"36"] || [provinciaCIF isEqualToString:@"27"] || [provinciaCIF isEqualToString:@"94"]) {
                provincia = @"Pontevedra";
            } else if ([provinciaCIF isEqualToString:@"37"]) {
                provincia = @"Salamanca";
            } else if ([provinciaCIF isEqualToString:@"38"] || [provinciaCIF isEqualToString:@"75"]) {
                provincia = @"Santa Cruz de Tenerife";
            } else if ([provinciaCIF isEqualToString:@"39"]) {
                provincia = @"Cantabria";
            } else if ([provinciaCIF isEqualToString:@"40"]) {
                provincia = @"Segovia";
            } else if ([provinciaCIF isEqualToString:@"41"] || [provinciaCIF isEqualToString:@"90"] || [provinciaCIF isEqualToString:@"91"]) {
                provincia = @"Sevilla";
            } else if ([provinciaCIF isEqualToString:@"42"]) {
                provincia = @"Soria";
            } else if ([provinciaCIF isEqualToString:@"43"] || [provinciaCIF isEqualToString:@"77"]) {
                provincia = @"Tarragona";
            } else if ([provinciaCIF isEqualToString:@"44"]) {
                provincia = @"Teruel";
            } else if ([provinciaCIF isEqualToString:@"45"]) {
                provincia = @"Toledo";
            } else if ([provinciaCIF isEqualToString:@"46"] || [provinciaCIF isEqualToString:@"96"] || [provinciaCIF isEqualToString:@"97"] || [provinciaCIF isEqualToString:@"98"]) {
                provincia = @"Valencia";
            } else if ([provinciaCIF isEqualToString:@"47"]) {
                provincia = @"Valladolid";
            } else if ([provinciaCIF isEqualToString:@"48"] || [provinciaCIF isEqualToString:@"95"]) {
                provincia = @"Vizcaya";
            } else if ([provinciaCIF isEqualToString:@"49"]) {
                provincia = @"Zamora";
            } else if ([provinciaCIF isEqualToString:@"50"] || [provinciaCIF isEqualToString:@"99"]) {
                provincia = @"Zaragoza";
            } else if ([provinciaCIF isEqualToString:@"51"]) {
                provincia = @"Ceuta";
            } else if ([provinciaCIF isEqualToString:@"52"]) {
                provincia = @"Melilla";
            }
            
            if ([provincia length]==0) {
                cifCorrecto = false;
                errorMsg = @"El codigo de provincia es inválido (2 digitos despues de la letra)";
            } else { // 2º Parte OK
                
                NSString *numeroCIF = [cif substringWithRange:NSMakeRange(1, 7)];
                
                if ([numeroCIF integerValue]>0) {
                    
                    //Suma de los pares
                    NSInteger sumPares = [[numeroCIF substringWithRange:NSMakeRange(1, 1)] integerValue] +
                    [[numeroCIF substringWithRange:NSMakeRange(3, 1)] integerValue] +
                    [[numeroCIF substringWithRange:NSMakeRange(5, 1)] integerValue];
                    
                    //Numeros impares x 2
                    NSString * dobleImpares1 = [NSString stringWithFormat:@"%i",[[numeroCIF substringWithRange:NSMakeRange(0, 1)] integerValue] * 2];
                    NSString * dobleImpares2 = [NSString stringWithFormat:@"%i",[[numeroCIF substringWithRange:NSMakeRange(2, 1)] integerValue] * 2];
                    NSString * dobleImpares3 = [NSString stringWithFormat:@"%i",[[numeroCIF substringWithRange:NSMakeRange(4, 1)] integerValue] * 2];
                    NSString * dobleImpares4 = [NSString stringWithFormat:@"%i",[[numeroCIF substringWithRange:NSMakeRange(6, 1)] integerValue] * 2];
                    
                    //Sumar las posiciones del resultado anterior
                    NSInteger posImpar1 = [[dobleImpares1 substringWithRange:NSMakeRange(0, 1)] integerValue];
                    if ([dobleImpares1 length]>1) {
                        posImpar1 = posImpar1 + [[dobleImpares1 substringWithRange:NSMakeRange(1, 1)] integerValue];
                    }
                    
                    NSInteger posImpar2 = [[dobleImpares2 substringWithRange:NSMakeRange(0, 1)] integerValue];
                    if ([dobleImpares2 length]>1) {
                        posImpar2 =posImpar2 + [[dobleImpares2 substringWithRange:NSMakeRange(1, 1)] integerValue];
                    }
                    
                    NSInteger posImpar3 = [[dobleImpares3 substringWithRange:NSMakeRange(0, 1)] integerValue];
                    if ([dobleImpares3 length]>1) {
                        posImpar3 = posImpar3 + [[dobleImpares3 substringWithRange:NSMakeRange(1, 1)] integerValue];
                    }
                    
                    NSInteger posImpar4 = [[dobleImpares4 substringWithRange:NSMakeRange(0, 1)] integerValue];
                    if ([dobleImpares4 length]>1) {
                        posImpar4 = posImpar4 + [[dobleImpares4 substringWithRange:NSMakeRange(1, 1)] integerValue];
                    }
                    
                    //Sumamos los impares
                    NSInteger sumImpar = posImpar1 + posImpar2 + posImpar3 + posImpar4;
                    
                    NSInteger sumTotal = sumPares + sumImpar;
                    
                    NSString * digitoUnidades = [[NSString stringWithFormat:@"%i",sumTotal] substringWithRange:NSMakeRange(1, 1)] ;
                    
                    NSInteger digitoControl = 10 - [digitoUnidades integerValue];
                    
                    if (digitoControl>=10) {
                        digitoControl = digitoControl - 10;
                    }
                    
                    NSString *numControlCIF = [cif substringWithRange:NSMakeRange(8, 1)];
                    
                    if ([numControlCIF isEqualToString:@"A"] || [numControlCIF isEqualToString:@"a"]) {
                        numControlCIF = @"1";
                    } else if ([numControlCIF isEqualToString:@"B"] || [numControlCIF isEqualToString:@"b"]) {
                        numControlCIF = @"2";
                    } else if ([numControlCIF isEqualToString:@"C"] || [numControlCIF isEqualToString:@"c"]) {
                        numControlCIF = @"3";
                    } else if ([numControlCIF isEqualToString:@"D"] || [numControlCIF isEqualToString:@"d"]) {
                        numControlCIF = @"4";
                    } else if ([numControlCIF isEqualToString:@"E"] || [numControlCIF isEqualToString:@"e"]) {
                        numControlCIF = @"5";
                    } else if ([numControlCIF isEqualToString:@"F"] || [numControlCIF isEqualToString:@"f"]) {
                        numControlCIF = @"6";
                    } else if ([numControlCIF isEqualToString:@"G"] || [numControlCIF isEqualToString:@"g"]) {
                        numControlCIF = @"7";
                    } else if ([numControlCIF isEqualToString:@"H"] || [numControlCIF isEqualToString:@"h"]) {
                        numControlCIF = @"8";
                    } else if ([numControlCIF isEqualToString:@"I"] || [numControlCIF isEqualToString:@"i"]) {
                        numControlCIF = @"9";
                    } else if ([numControlCIF isEqualToString:@"J"] || [numControlCIF isEqualToString:@"j"]) {
                        numControlCIF = @"0";
                    }
                    
                    if ([numControlCIF integerValue] == digitoControl) {
                        //CIF OK
                        ;
                    } else {
                        cifCorrecto = false;
                        errorMsg = @"El codigo de Control (Ultima posicion) no és válido";
                    }
                    
                    
                } else {
                    cifCorrecto = false;
                    errorMsg = @"Si el CIF es 123456789, la parte 45678 debe ser númerica";
                }
                
                
            }
            
        }
        
        
        
    } else {
        //El CIF debe tener 9 digitos
        cifCorrecto = false;
        errorMsg = @"El CIF debe tener 9 digitos";
    }
    
    
    //Configurar NSError
    if ([errorMsg length]>0) {
        NSMutableDictionary *errorDetail = [NSMutableDictionary dictionary];
        [errorDetail setValue:errorMsg forKey:NSLocalizedDescriptionKey];
        *error = [NSError errorWithDomain:@"errorCIF" code:100 userInfo:errorDetail];
    }
    
    
    return cifCorrecto;
}

//+ (BOOL)isCIFValid:(NSString *)cif
//{
//    // Array de Letras válidas para el CIF
//    NSArray * letters = @[@"-", @"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"K", @"L", @"M", @"N", @"P", @"Q", @"S"];
//    
//    DLog(@"Validating CIF: %@", cif);
//    if (cif.length == 9){
//        
//        NSMutableArray *integers = [[NSMutableArray alloc] init];
//        
//        [integers addObject:[NSNumber numberWithInt:[cif characterAtIndex:1] - '0']];
//        [integers addObject:[NSNumber numberWithInt:[cif characterAtIndex:2] - '0']];
//        [integers addObject:[NSNumber numberWithInt:[cif characterAtIndex:3] - '0']];
//        [integers addObject:[NSNumber numberWithInt:[cif characterAtIndex:4] - '0']];
//        [integers addObject:[NSNumber numberWithInt:[cif characterAtIndex:5] - '0']];
//        [integers addObject:[NSNumber numberWithInt:[cif characterAtIndex:6] - '0']];
//        [integers addObject:[NSNumber numberWithInt:[cif characterAtIndex:7] - '0']];
//        
//        int sumaA = [[integers objectAtIndex:1] intValue] + [[integers objectAtIndex:3] intValue] + [[integers objectAtIndex:5] intValue];
//        int sumaB = 0;
//        
//        for (unsigned j = 0; j<[integers count]; j++){
//            if (j%2 == 0){
//                int num = [[integers objectAtIndex:j] intValue];
//                DLog(@"Operación para Suma B: %d * 2 = %d", num, num*2);
//                num = num * 2;
//                while (num){
//                    DLog(@"Sumamos a B: %d", num%10);
//                    sumaB += num%10;
//                    num /= 10;
//                }
//            }
//        }
//        int sumaC = sumaA + sumaB;
//        
//        DLog(@"Suma A: %d", sumaA);
//        DLog(@"Suma B: %d", sumaB);
//        DLog(@"Suma C: %d", sumaC);
//        
//        NSString * numString = [NSString stringWithFormat:@"%d", sumaC];
//        unichar unidad = [numString characterAtIndex:[numString length]-1];
//        if (isnumber(unidad)){
//            NSNumber * unidadNumber = [NSNumber numberWithInt:unidad - '0'];
//            int digitoControl = 10 - [unidadNumber intValue];
//            DLog(@"Digito de Control: %d", digitoControl);
//            
//            unichar cifNumControl = [cif characterAtIndex:[cif length]-1];
//            if (isnumber(cifNumControl)){
//                DLog(@"El dígito de control del CIF introducido es %d y nosotros hemos obtenido %d", cifNumControl - '0', digitoControl);
//                if (cifNumControl-'0' == digitoControl){
//                    DLog(@"CIF Válido");
//                    return YES;
//                }else{
//                    DLog(@"CIF Inválido");
//                    return NO;
//                }
//            }else{
//                if ([cif hasSuffix:[letters objectAtIndex:digitoControl]]){
//                    DLog(@"CIF Válido letra");
//                    return YES;
//                }else{
//                    DLog(@"CIF Inválido letra");
//                    return NO;
//                }
////                DLog(@"El dígito de control del CIF introducido es %hu y nosotros hemos obtenido %d que viene a ser %@", cifNumControl, digitoControl, [letters objectAtIndex:digitoControl]);
//            }
//        }else{
//            return NO;
//        }
//        
//        
//    }
//    
//    return NO;
//}

+(NSString*) checkDocumentLetter:(NSString*) numbers {
 NSArray *validDC =@[@"T",@"R",@"W",@"A",@"G",@"M",@"Y",@"F",@"P",@"D",@"X",@"B",@"N",@"J",@"Z",@"S",@"Q",@"V",@"H",@"L",@"C",@"K",@"E"];
     NSString* correctLetter = [validDC objectAtIndex:[numbers intValue]%23];
    return correctLetter;

}


+(BOOL)areAllCharactersOfThisString:(NSString *)string inThisCollection:(NSString *)characters{
    
    NSCharacterSet *unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:characters] invertedSet];
    
    // Create array of strings from incoming string using the unacceptable
    // characters as the trigger of where to split the string.
    // If array has more than one entry, there was at least one unacceptable character
    if ([[string componentsSeparatedByCharactersInSet:unacceptedInput] count] > 1){
        return NO;
    }
    
    return YES;
}

@end
